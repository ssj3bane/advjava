package tcayer.classwork.GeoFigure;

public class Square extends GeoFigure implements SidedObject{
	public Square() {
		setFigureType("Square");
		setSides(4);
	}
	
	public void setLength(int length) {
		setWidth(length);
		setHeight(length);
	}
}
