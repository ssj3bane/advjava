package tcayer.classwork.GeoFigure;

import tcayer.improvements.Print;

public interface SidedObject {
	int[] sides = new int[] {0};
	default void setSides(int sides) {
		this.sides[0] = sides;
	}
	default void displaySides() {
		Print.ln(this.sides[0]);
	}
}
