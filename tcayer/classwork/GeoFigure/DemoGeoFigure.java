package tcayer.classwork.GeoFigure;

import tcayer.improvements.Print;

public class DemoGeoFigure {
	public static void main(String[] args) {
		Square mySquare = new Square();
		mySquare.setLength(10);
		Print.ln("Display mySquare side count");
		mySquare.displaySides();
		mySquare.updateArea();
		Print.ln("mySquare.toString()");
		Print.ln(mySquare.toString());
		Square mySquare2 = new Square();
		mySquare2.setLength(10);
		Print.ln("Display mySquare2 side count");
		mySquare2.displaySides();
		mySquare2.updateArea();
		Print.ln("mySquare2.toString()");
		Print.ln(mySquare2.toString());
		Triangle myTriangle = new Triangle();
		myTriangle.setWidth(10);
		myTriangle.setHeight(10);
		Print.ln("Display myTriangle side count");
		myTriangle.displaySides();
		myTriangle.updateArea();
		Print.ln("myTriangle.toString()");
		Print.ln(myTriangle.toString());
		Print.ln("mySquare comparison to mySquare2");
		Print.ln(mySquare.equals(mySquare2));
		Print.ln("mySquare2 comparison to myTriangle");
		Print.ln(mySquare2.equals(myTriangle));
	}
}
