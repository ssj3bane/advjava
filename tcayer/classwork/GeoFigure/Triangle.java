package tcayer.classwork.GeoFigure;

public class Triangle extends GeoFigure implements SidedObject{
	public Triangle() {
		setFigureType("Triangle");
		setSides(3);
	}
}
