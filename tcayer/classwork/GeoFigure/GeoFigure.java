package tcayer.classwork.GeoFigure;

public abstract class GeoFigure {
	private int height;
	private int width;
	private String figureType;
	private int area;
	public GeoFigure() {
		setHeight(0);
		setWidth(0);
		setFigureType("");
		updateArea();
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public String getFigureType() {
		return figureType;
	}
	public void setFigureType(String figureType) {
		this.figureType = figureType;
	}
	public int getArea() {
		return area;
	}
	public void setArea(int area) {
		this.area = area;
	}
	public void updateArea() {
		if(getFigureType().toLowerCase().equals("triangle")) {
			this.area = (getWidth() * getHeight()) / 2;
		} else if(getFigureType().toLowerCase().equals("square")) {
			this.area = getWidth() * getHeight();
		}
	}
	public String toString() {
		
		return "Width: " + getWidth() +
				"\nHeight: " + getHeight() +
				"\nFigure Type: " + getFigureType() +
				"\nArea: " + getArea();
	}
	public boolean equals(GeoFigure compareTo) {
		boolean rv = true;
		if(getWidth() != compareTo.getWidth()	||
			getHeight() != compareTo.getHeight()||
			getArea() != compareTo.getArea()	||
			!getFigureType().equals(compareTo.getFigureType()))
			rv = false;
		return rv;
	}
}
