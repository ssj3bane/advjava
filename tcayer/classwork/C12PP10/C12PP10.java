package tcayer.classwork.C12PP10;

/**
 * TO THE PROFESSOR
 * There is confusion in the method inSameGroup
 * Please contact me to make sure that I understand the question as it stands
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;
import java.util.function.Consumer;

public class C12PP10 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		GroupHolder groupHolder = new GroupHolder();
		// Create choice structure
		// Options include adding an item, creating a union, and checking for same groups
		// As well as viewing representatives
		String lastCommand = "";
		while(!lastCommand.toLowerCase().equals("quit")) {
			System.out.println("Valid commands: add, union, representative, view all, same group, quit\nEnter a command: ");
			if(input.hasNextLine())
				lastCommand = input.nextLine();
			switch(lastCommand.toLowerCase()) {
			case "add":
				addItem(groupHolder, input);
				break;
			case "union":
				union(groupHolder, input);
				break;
			case "representative":
				representative(groupHolder, input);
				break;
			case "same group":
				sameGroup(groupHolder, input);
				break;
			case "view all":
				System.out.println(groupHolder.getAllRepresentatives());
				break;
			case "quit":
				System.exit(0);
				break;
			default:
				System.out.println(lastCommand + " is not a valid command.");
				break;
			}
		}
	}
	public static void addItem(GroupHolder gh, Scanner sc) {
		String item = null;
		System.out.println("Enter a string: ");
		if(sc.hasNextLine())
			item = sc.nextLine();
		gh.addItem(item);
	}
	public static void union(GroupHolder gh, Scanner sc) {
		String item1 = null;
		String item2 = null;
		System.out.println("Enter node1: ");
		if(sc.hasNextLine())
			item1 = sc.nextLine();
		System.out.println("Enter node2: ");
		if(sc.hasNextLine())
			item2 = sc.nextLine();
		gh.union(item1, item2);
	}
	public static void representative(GroupHolder gh, Scanner sc) {
		String item = null;
		System.out.println("Enter a node: ");
		if(sc.hasNextLine())
			item = sc.nextLine();
		System.out.println(gh.getRepresentative(item));
	}
	public static void sameGroup(GroupHolder gh, Scanner sc) {
		String item1 = null;
		String item2 = null;
		System.out.println("Enter node1: ");
		if(sc.hasNextLine())
			item1 = sc.nextLine();
		System.out.println("Enter node2: ");
		if(sc.hasNextLine())
			item2 = sc.nextLine();
		System.out.println("Is "+item1+" and "+item2+" in the same group?: "+gh.inSameGroup(item1, item2));
	}
}
class GroupHolder {
	private ArrayList<GroupNode> items = new ArrayList<GroupNode>();
	private HashMap<String, Boolean> boolMap = new HashMap<String, Boolean>();
	public void addItem(String s) {
		boolMap.put("node exists", false);
		GroupNode potentialNode = new GroupNode(s);
		items.forEach(new AddItemConsumer(potentialNode));
		if(boolMap.get("node exists").booleanValue()) {
			//Node exists already; do nothing
			System.out.println("Node " + s + " already exists");
		} else {
			//Node does not yet exist; add it
			System.out.println("Node " + s + " added to items");
			items.add(potentialNode);
		}
		boolMap.put("node exists", false);
	}
	public String getRepresentative(String s) {
		Boolean foundNode = false;
		GroupNode curNode = null;
		Iterator<GroupNode> itemIt = items.iterator();
		while(itemIt.hasNext() && !foundNode) {
			GroupNode viewingNode = itemIt.next();
			if(viewingNode.data.equals(s)) {
				curNode = viewingNode;
				foundNode = true;
			}
		}
		if(curNode == null) {
			System.out.println("No node with the data " + s + " exists");
			return null;
		} else {
			String output = null;
			if(curNode.link != null) {
				return getRepresentative(curNode.link.data);
			} else {
				return curNode.data;
			}
		}
	}
	public ArrayList<String> getAllRepresentatives() {
		ArrayList<String> representatives = new ArrayList<String>();
		items.forEach(new RepresentativesConsumer(representatives));
		return representatives;
	}
	public Boolean inSameGroup(String s1, String s2) {
		/**
		 * THIS IS AN AREA OF CONCERN
		 * There is a clerical error in the book
		 * It says that if they have the same representative, the nodes are in the same group
		 * This makes sense in the case of {a, b, c, d}, with [a] and [d] being in the same group
		 * However, it also states that the link must not be null for either.
		 * If [d] is the representative of the group, it is surely, logically, in the same group as [a],
		 * though with the books reasoning, they are not, due to [d] having a null link
		 */
		return getRepresentative(s1).equals(getRepresentative(s2)) && (getNode(s1).link != null && getNode(s2).link != null);
	}
	public void union(String s1, String s2) {
		GroupNode node1 = getNode(s1);
		GroupNode node2 = getNode(s2);
		if(node1 != null && node2 != null)
			node1.link = node2;
	}
	private GroupNode getNode(String s) {
		Boolean foundNode = false;
		GroupNode curNode = null;
		Iterator<GroupNode> itemIt = items.iterator();
		while(itemIt.hasNext() && !foundNode) {
			GroupNode viewingNode = itemIt.next();
			if(viewingNode.data.equals(s)) {
				curNode = viewingNode;
				foundNode = true;
			}
		}
		if(!foundNode) {
			System.out.println("Node does not exist");
			return null;
		} else {
			return curNode;
		}
	}
	public void viewAll() {
		Iterator<GroupNode> itemIt = items.iterator();
		while(itemIt.hasNext()) {
			GroupNode item = itemIt.next();
			item.viewData();
		}
	}
	class GroupNode {
		private String data;
		private GroupNode link = null;
		public GroupNode(String s) {
			data = s;
		}
		public void viewData() {
			System.out.println(data);
			if(link != null) {
				link.viewData();
				System.out.println("is linked to");
			} else {
				System.out.println("This is a representative");
			}
		}
	}
	class AddItemConsumer implements Consumer<GroupNode> {
		private GroupNode nodeBeingChecked;
		public AddItemConsumer(GroupNode potNode) {
			nodeBeingChecked = potNode;
		}
		@Override
		public void accept(GroupNode t) {
			if(nodeBeingChecked.data.equals(t.data)) {
				boolMap.put("node exists", true);
			}
		}
	}
	class RepresentativesConsumer implements Consumer<GroupNode> {
		private ArrayList<String> nodeList;
		public RepresentativesConsumer(ArrayList<String> representatives) {
			nodeList = representatives;
		}
		@Override
		public void accept(GroupNode t) {
			if(t.link == null) {
				nodeList.add(t.data);
			}
		}
	}
}
