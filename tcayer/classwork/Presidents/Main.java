package tcayer.classwork.Presidents;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
	public static void main(String[] args) throws FileNotFoundException {
		ArrayList<President> presidents = new ArrayList<President>();
		Scanner inputStream = new Scanner(new FileInputStream("PresidentsTerms.csv"));
		boolean isFirstLine = true;
        while(inputStream.hasNextLine()) {
        	if(!isFirstLine) {
	        	String presidentInfo = inputStream.nextLine();
	        	String[] arr = presidentInfo.split(",");
	        	String[] years = arr[1].split("-");
	        	if(years.length == 1) {
	        		presidents.add(new President(arr[0], Integer.parseInt(years[0]), Integer.parseInt(years[0])));
	        	} else if(years.length == 2) {
	        		presidents.add(new President(arr[0], Integer.parseInt(years[0]), Integer.parseInt(years[1])));
	        	}
        	} else {
        		isFirstLine = false;
        		inputStream.nextLine();
        	}
        }
        inputStream.close();
        for(President p : presidents) {
        	System.out.println(p.getFormattedInfo());
        }
	}
}

class President {
	public String name;
	public int startTerm;
	public int endTerm;
	public President(String name, int startTerm, int endTerm) {
		this.name = name;
		this.startTerm = startTerm;
		this.endTerm = endTerm;
	}
	public String getFormattedInfo() {
		return "Name: " + getName() + "\n\tFirst year in office: " + getStartTerm() + "\n\tLast year in office: " + getEndTerm() + "\n\tYears served: " + getYearsServed();
	}
	private int getEndTerm() {
		return this.endTerm;
	}
	private int getStartTerm() {
		return this.startTerm;
	}
	private String getName() {
		return this.name;
	}
	private int getYearsServed() {
		return (getEndTerm() - getStartTerm()) != 0 ? getEndTerm() - getStartTerm() : 1;
	}
}