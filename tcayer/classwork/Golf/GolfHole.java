package tcayer.classwork.Golf;

public class GolfHole {
	private int number;
	private int par;
	private int handicap;
	private int score;
	private int distance;
	private String description;

	public GolfHole() {
		number = 0;
		par = 0;
		handicap = 0;
		score = 0;
		distance = 0;
		description = "Not yet assigned";
	}
	
	GolfHole(int numberPassed, int parPassed, int handicapPassed, int scorePassed, int distancePassed, String descriptionPassed) {
		this.setNumber(numberPassed);
		this.setPar(parPassed);
		this.setHandicap(handicapPassed);
		this.setScore(scorePassed);
		this.setDistance(distancePassed);
		this.setDescription(descriptionPassed);
	}
	
	public int getNumber() {
		return number; 
	}

	public int getPar() {
		return par; 
	}

	public int getHandicap() {
		return handicap;
	}

	public int getScore() {
		return score; 
	}

	public int getDistance() {
		return distance; 
	}

	public String getDescription() {
		return description; 
	}

	public void setNumber(int numberPassed) {
		if(numberPassed < 1 || numberPassed > 18) {
			System.out.println("Invalid input hole number must be 1 - 18 inclusive.");
			System.out.println("Hole number will be set to 0");
			numberPassed = 0;
		}
		number = numberPassed;
	}

	public void setPar(int parPassed) {
		if(parPassed < 1 || parPassed > 5) {
			System.out.println("Invalid input par must be 1 - 5 inclusive.");
			System.out.println("Par will be set to 0");
			parPassed = 0;
		}
		par = parPassed;
	}
	
	public void setHandicap(int handicapPassed) {
		if(handicapPassed < 1 || handicapPassed > 18) {
			System.out.println("Invalid handicap must be 1 - 18 inclusive.");
			System.out.println("Handicap will be set to 18");
			handicapPassed = 18;
		}
		handicap = handicapPassed;  
	}

	public void setScore(int scorePassed) {
		if(scorePassed < 1 || scorePassed > 10) {
			System.out.println("Invalid score must be 1 - 10 inclusive.");
			System.out.println("Score will be set to 10");
			scorePassed = 10;
		}
		score = scorePassed;
	}

	public void setDistance(int distancePassed) {
		if(distancePassed < 0) {
			System.out.println("Invalid distance must be positive.");
			System.out.println("Distance will be set to 325");
			distancePassed = 325;
		}
		distance = distancePassed;
	}

	public void setDescription(String descriptionPassed) {
		description =  descriptionPassed;
	}

	public String toString() {
		return "Hole information\n"
				+ "\n Number : " + number
				+ "\n Par : " + par
				+ "\n Handicap : " + handicap
				+ "\n Score : " + score
				+ "\n Distance : " + distance
				+ "\n Description : " + description + "\n";
	}
}