package tcayer.classwork.Golf;

import java.util.Scanner;

public class RoundOfGolf {
	public static Scanner input;
	public static final String[] MenuItem = new String[] {
			"Calculate and print the total score for the round",
			"Calculate and print the number of strokes above/below par for the round", 
			"Calculate and print the number of aces (hole in one)",
			"Calculate and print the number of condors (4 below par)",
			"Calculate and print the number of albatross (3 below par)",
			"Calculate and print the number of eagles (2 below par)",
			"Calculate and print the number of birdies (one below par)",
			"Calculate and print the number of pars (at par)",
			"Calculate and print the number of bogeys (one above par)",
			"Calculate and print the number of double bogeys (two above par)",
			"Calculate and print the number of triple bogeys (three above par)",
			"Print the hole information for the holes on which they scored aces",
			"Print the hole information for the holes on which they scored condors",
			"Print the hole information for the holes on which they scored albatrossí",
			"Print the hole information for the holes on which they scored eagles",
			"Print the hole information for the holes on which they scored birdies",
			"Print the hole information for the holes on which they scored pars",
			"Print the hole information for the holes on which they scored bogeys",
			"Print the hole information for the holes on which they scored double bogeys",
			"Print the hole information for the holes on which they scored triple bogeys",
			"Select again",
			"Exit the program"	
	};
	public static GolfHole[] holes = new GolfHole[] {
		new GolfHole(1, 4, 4, 1, 433, "The hole is a simple straight shot over a slight hill."),
		new GolfHole(2, 4, 2, 1, 418, "The hole is around the corner of a bend."),
		new GolfHole(3, 3, 12, 1, 166, "The hole is on the other side of a lake."),
		new GolfHole(4, 4, 18, 1, 344, "The hole is a simple straight shot over a slight hill."),
		new GolfHole(5, 4, 16, 1, 408, "The hole is around the corner of a bend."),
		new GolfHole(6, 5, 6, 1, 545, "The hole is on the other side of a lake."),
		new GolfHole(7, 4, 10, 1, 378, "The hole is a simple straight shot over a slight hill."),
		new GolfHole(8, 3, 8, 1, 204, "The hole is around the corner of a bend."),
		new GolfHole(9, 5, 14, 1, 476, "The hole is on the other side of a lake."),
		new GolfHole(10, 4, 3, 1, 455, "The hole is a simple straight shot over a slight hill."),
		new GolfHole(11, 4, 11, 1, 397, "The hole is around the corner of a bend."),
		new GolfHole(12, 5, 7, 1, 555, "The hole is on the other side of a lake."),
		new GolfHole(13, 3, 5, 1, 238, "The hole is a simple straight shot over a slight hill."),
		new GolfHole(14, 4, 13, 1, 383, "The hole is around the corner of a bend."),
		new GolfHole(15, 4, 1, 1, 389, "The hole is on the other side of a lake."),
		new GolfHole(16, 3, 17, 1, 137, "The hole is a simple straight shot over a slight hill."),
		new GolfHole(17, 4, 15, 1, 341, "The hole is around the corner of a bend."),
		new GolfHole(18, 5, 9, 1, 523, "The hole is on the other side of a lake."),
	};
	public static String lastCommand = "";
	public static void main(String[] args) {
		setup();
		help();
		trueMain();
	}
	public static void setup() {
		input = new Scanner(System.in);
		for(int i = 0; i < holes.length; i++)
			holes[i].setScore((int) Prompt.prompt("Enter your score for hole " + holes[i].getNumber() + ":", input, 1));
	}
	public static void parseString(String letter) {
		switch(letter.toLowerCase()) { 
			case "a":
				calcTotal(holes);
				break;
			case "b":
				calcNonPar(holes);
				break;
			case "c":
				calcHole(holes, 5);
				break;
			case "d":
				calcHole(holes, 4);
				break;
			case "e":
				calcHole(holes, 3);
				break;
			case "f":
				calcHole(holes, 2);
				break;
			case "g":
				calcHole(holes, 1);
				break;
			case "h":
				calcHole(holes, 0);
				break;
			case "i":
				calcHole(holes, -1);
				break;
			case "j":
				calcHole(holes, -2);
				break;
			case "k":
				calcHole(holes, -3);
				break;
			case "l":
				calcHoleInfo(holes, 5); // ACE
				break;
			case "m":
				calcHoleInfo(holes, 4); // CONDOR
				break;
			case "n":
				calcHoleInfo(holes, 3); // ALBATROSS
				break;
			case "o":
				calcHoleInfo(holes, 2); // EAGLE
				break;
			case "p":
				calcHoleInfo(holes, 1); // BIRDIE
				break;
			case "q":
				calcHoleInfo(holes, 0); // PAR
				break;
			case "r":
				calcHoleInfo(holes, -1); // BOGEY
				break;
			case "s":
				calcHoleInfo(holes, -2); // DOUBLE BOGEY
				break;
			case "t":
				calcHoleInfo(holes, -3); // TRIPLE BOGEY
				break;
			case "u":
				if(!lastCommand.toLowerCase().equals("u")) {
					Print.ln("");
					parseString(lastCommand);
				}
				break;
			case "v":
				System.exit(0);
				break;
		}
		lastCommand = letter;
	}
	private static void calcTotal(GolfHole[] round) {
		int score = 0;
		for(GolfHole hole : round)
			score += hole.getScore();
		Print.ln("Total score : " + score);
	}
	private static void calcNonPar(GolfHole[] round) {
		int score = 0;
		for(GolfHole hole : round)
			score += hole.getScore() - hole.getPar();
		if(score < 0)
			Print.ln(score + " below round par.");
		else
			Print.ln(score + " above round par.");
	}
	private static void calcHole(GolfHole[] round, int parSelector) {
		int holesHit = 0;
		for(GolfHole hole : round)
			if(hole.getScore() - hole.getPar() == parSelector)
				holesHit++;
		Print.ln(holesHit + " holes hit.");
	}
	private static void calcHoleInfo(GolfHole[] round, int parSelector) {
		for(GolfHole hole : round)
			if(hole.getScore() - hole.getPar() == parSelector)
				Print.ln(hole.toString());
	}
	public static void help() {
		Print.ln(
			"a. " + MenuItem[0] +
			"\nb. " + MenuItem[1] + 
			"\nc. " + MenuItem[2] +
			"\nd. " + MenuItem[3] +
			"\ne. " + MenuItem[4] +
			"\nf. " + MenuItem[5] +
			"\ng. " + MenuItem[6] +
			"\nh. " + MenuItem[7] +
			"\ni. " + MenuItem[8] +
			"\nj. " + MenuItem[9] +
			"\nk. " + MenuItem[10] +
			"\nl. " + MenuItem[11] +
			"\nm. " + MenuItem[12] +
			"\nn. " + MenuItem[13] +
			"\no. " + MenuItem[14] +
			"\np. " + MenuItem[15] +
			"\nq. " + MenuItem[16] +
			"\nr. " + MenuItem[17] +
			"\ns. " + MenuItem[18] +
			"\nt. " + MenuItem[19] +
			"\nu. " + MenuItem[20] +
			"\nv. " + MenuItem[21]
		);
	}
	public static void trueMain() {
		if(!lastCommand.toLowerCase().equals("quit")) {
			parseString((String) Prompt.prompt("Do what?: ", input, "line"));
			trueMain();
		}
	}
}

/*
 * Part of my improvements library
 */

class Print {
	public static void ln(Object arg0) {
		System.out.println(arg0);
	}
	public static void ln(Object[] arg0) {
		System.out.println(arg0);
	}
	public static void p(Object arg0) {
		System.out.print(arg0);
	}
	public static void p(Object[] arg0) {
		System.out.print(arg0);
	}
}

class Prompt {
	public static Object prompt(String _prompt, Scanner input, Object type) {
		Print.p(_prompt);
		Object rv = new Error();
		boolean t = true;
		if(type instanceof Integer)
			if(input.hasNextInt())
				rv = input.nextInt();
		else if(type instanceof String)
			if(((String)type).toLowerCase().equals("line"))
				if(input.hasNextLine())
					rv = input.nextLine();
			else
				if(input.hasNext())
					rv = input.next();
		else if(type instanceof Boolean)
			if(input.hasNextBoolean())
				rv = input.nextBoolean();
		else if(type instanceof Float)
			if(input.hasNextFloat())
				rv = input.nextFloat();
		else if(type instanceof Double)
			if(input.hasNextDouble())
				rv = input.nextDouble();
		while(rv instanceof Error && t) {
			if(type instanceof Integer)
				rv = input.nextInt();
			else if(type instanceof String)
				rv = input.next();
			else if(type instanceof Boolean)
				rv = input.nextBoolean();
			else if(type instanceof Float)
				rv = input.nextFloat();
			else if(type instanceof Double)
				rv = input.nextDouble();
			t = false;
		}
		return rv;
	}
}