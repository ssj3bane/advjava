package tcayer.classwork.FileWriter;

import java.util.Scanner;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;

public class FileWriter {
    public static void main(String[] args) throws FileNotFoundException, ClassNotFoundException {
        Scanner inputStream = null;
        PrintWriter outputStream = null;
        inputStream = new Scanner(new FileInputStream("binary.txt"));
        outputStream = new PrintWriter(new FileOutputStream("stage.txt"));
        int counter = 0;
        String byteHolder = "";
        while(inputStream.hasNextLine()) {
        	String byteLine = inputStream.nextLine();
        	String lastBit = Character.toString(byteLine.charAt(byteLine.length()-1));
    		byteHolder += lastBit;
        	if(counter > 6) {
        		byteHolder += "\n";
        		counter = 0;
        	} else {
        		counter++;
        	}
        }
        outputStream.write(byteHolder);
        inputStream.close();
        outputStream.close();
    }
}