package tcayer.classwork.C11PP8;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class C11PP8 {
	public static void main(String[] args) throws FileNotFoundException {
		List<String> dictionary = new ArrayList<String>();
		Scanner inputStream = new Scanner(new FileInputStream("words.txt"));
		while (inputStream.hasNextLine()) {
			String word = inputStream.nextLine().toLowerCase();
			dictionary.add(word);
		}
		inputStream.close();
		Scanner input = new Scanner(System.in);
		String typed = "";
		System.out.println("Type !QUIT! to quit\nOtherwise, type a word to start the ladder");
		while (!typed.equals("!QUIT!")) {
			if (input.hasNextLine())
				typed = input.nextLine();
			String _targetWord = null;
			System.out.println("Enter a target word: ");
			if (input.hasNextLine())
				_targetWord = input.nextLine();
			if (_targetWord != null && typed != null) {
				System.out.println("Word ladder length: " + wordLadder(typed, _targetWord, dictionary));
			}
			// Quit after 1 try, due to dictionary size, program is very memory intensive
			input.close();
			System.exit(0);
		}
	}

	public static List<List<String>> wordLadder(String sourceWord, String targetWord, List<String> myDictionary) {
		Set<String> dict = new HashSet<String>(myDictionary);
		List<List<String>> paths = new ArrayList<List<String>>();
		if (!dict.contains(targetWord))
			return paths;
		LinkedList<WordNode> queue = new LinkedList<WordNode>();
		queue.add(new WordNode(sourceWord, 1, null));

		HashSet<String> checked = new HashSet<String>();
		HashSet<String> needToCheck = new HashSet<String>();
		needToCheck.addAll(dict);
		int lastDistance = 0;

		while (!queue.isEmpty()) {
			WordNode curWord = queue.remove();
			String word = curWord.word;
			int currDist = curWord.dist;
			if (word.equals(targetWord)) {
				ArrayList<String> potentialPath = new ArrayList<String>();
				potentialPath.add(curWord.word);
				while (curWord.prev != null) {
					potentialPath.add(0, curWord.prev.word);
					curWord = curWord.prev;
				}
				paths.add(potentialPath);
				continue;
			}
			if (lastDistance < currDist)
				needToCheck.removeAll(checked);
			lastDistance = currDist;
			char[] wordAsChars = word.toCharArray();
			for (int index = 0; index < wordAsChars.length; index++) {
				for (char modCharacter = 'a'; modCharacter <= 'z'; modCharacter++) {
					char temp = wordAsChars[index];
					if (wordAsChars[index] != modCharacter) {
						wordAsChars[index] = modCharacter;
					}
					String reformedWord = new String(wordAsChars);
					if (needToCheck.contains(reformedWord)) {
						queue.add(new WordNode(reformedWord, curWord.dist + 1, curWord));
						checked.add(reformedWord);
					}
					wordAsChars[index] = temp;
				}
			}
		}
		return paths;
	}
}

class WordNode {
	public String word;
	public int dist;
	public WordNode prev;

	public WordNode(String word, int dist, WordNode prev) {
		this.word = word;
		this.dist = dist;
		this.prev = prev;
	}
}
