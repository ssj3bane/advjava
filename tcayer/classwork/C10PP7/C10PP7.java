package tcayer.classwork.C10PP7;

import java.util.function.Consumer;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class C10PP7 {
	// Ask if want to read or write
	// Ask for file name
	// If write create pet
	// If read print 5 lines then wait for input
	public static void main(String[] args) throws FileNotFoundException {
		Scanner input = new Scanner(System.in);
		String lastCommand = "";
		System.out.println("Would you like to read or write?");
		while(!lastCommand.toLowerCase().equals("read") &&
				!lastCommand.toLowerCase().equals("write")) {
			lastCommand = input.nextLine();
		}
		System.out.println("Enter a file name");
		if(lastCommand.toLowerCase().equals("read")) {
			String fileName = "";
			if(input.hasNextLine()) {
				fileName = input.nextLine();
				ArrayList<Object> tempArray = Serializer.readFile(fileName);
				ArrayList<Pet> petList = new ArrayList<Pet>();
				tempArray.forEach(new ObjectToPetConsumer(petList));
				petList.forEach(new ReadConsumer());
			}
		} else if(lastCommand.toLowerCase().equals("write")) {
			String fileName = "";
			if(input.hasNextLine()) {
				fileName = input.nextLine();
				System.out.println("Enter number of pets: ");
				if(input.hasNextInt()) {
					int petNum = input.nextInt();
					ArrayList<Pet> petList = new ArrayList<Pet>();
					System.out.println("Enter pet info\nFailure to match data types will result in program closure!");
					for(int i = 0; i < petNum; i++) {
						System.out.println("Enter name:");
						String name = "";
						int age = 0;
						double weight = 0;
						input.nextLine();
						if(input.hasNextLine())
							name = input.nextLine();
						System.out.println("Enter age:");
						if(input.hasNextInt())
							age = input.nextInt();
						System.out.println("Enter weight:");
						if(input.hasNextDouble())
							weight = input.nextDouble();
						petList.add(new Pet(name, age, weight));
					}
					System.out.println("Writing to file " + fileName);
					Serializer.writeFile(fileName, petList);
				}
			}
		}
		input.close();
	}
}
class Serializer {
	public static ArrayList<Object> readFile(String fileName) throws FileNotFoundException {
		ArrayList<Object> data = new ArrayList<Object>();
		Scanner inputStream = new Scanner(new FileInputStream(fileName));
        while(inputStream.hasNextLine()) {
        	String line = inputStream.nextLine();
        	data.add(line);
        }
        inputStream.close();
		return data;
	}
	public static void writeFile(String fileName, ArrayList<Pet> petList) throws FileNotFoundException {
        PrintWriter outputStream = new PrintWriter(new FileOutputStream(fileName));
        petList.forEach(new WriteConsumer(outputStream));
        outputStream.close();
	}
}
class WriteConsumer implements Consumer<Object> {
	private PrintWriter writer;
	public WriteConsumer(PrintWriter _writer) {
		writer = _writer;
	}
	@Override
	public void accept(Object arg0) {
		writer.write(((Pet) arg0).getFormattedData()+"\n");
	}
}
class ReadConsumer implements Consumer<Object> {
	@Override
	public void accept(Object arg0) {
		((Pet) arg0).writeOutput();
	}
}
class ObjectToPetConsumer implements Consumer<Object> {
	ArrayList<Pet> arrayHolder;
	public ObjectToPetConsumer(ArrayList<Pet> petList) {
		arrayHolder = petList;
	}
	@Override
	public void accept(Object arg0) {
		arrayHolder.add(Pet.createFromData((String) arg0));
	}
}
/*
 * Page 822
 * From listing 6.1
 * Revise and make serializable
 * Serialize, ask read write file, ask for file name
 * Planned structure, comma-separated, data surrounded by double quotes
 */
class Pet {
	private String name;
	private int age;
	private double weight;
	public Pet() {
		name = "No name yet";
		age = 0;
		weight = 0;
	}
	public Pet(String initialName, int initialAge, double initialWeight) {
		name = initialName;
		if((initialAge < 0) || (initialWeight < 0)) {
			System.out.println("Error: Negative age or weight.");
			System.exit(0);
		} else {
			age = initialAge;
			weight = initialWeight;
		}
	}
	public void setPet(String newName, int newAge, double newWeight) {
		name = newName;
		if(newAge < 0 || newWeight < 0) {
			System.out.println("Error: Negative age  or weight");
			System.exit(0);
		} else {
			age = newAge;
			weight = newWeight;
		}
	}
	public Pet(String initialName) {
		name = initialName;
		age = 0;
		weight = 0;
	}
	public void setName(String newName) {
		name = newName;
	}
	public Pet(int initialAge) {
		name = "No name yet";
		weight = 0;
		if(initialAge < 0) {
			System.out.println("Error: Negative age");
			System.exit(0);
		} else {
			age = initialAge;
		}
	}
	public void setAge(int newAge) {
		age = newAge;
	}
	public Pet(double initialWeight) {
		name = "No name yet";
		age = 0;
		if(initialWeight < 0) {
			System.out.println("Error: Negative weight");
			System.exit(0);
		} else {
			weight = initialWeight;
		}
	}
	public void setWeight(double newWeight) {
		weight = newWeight;
	}
	public String getName() {
		return name;
	}
	public int getAge() {
		return age;
	}
	public double getWeight() {
		return weight;
	}
	public void writeOutput() {
		System.out.println("Name: " + name + "\nAge: " + age + "\nWeight: " + weight);
	}
	public String getFormattedData() {
		return "name:"+getName()+",age:"+getAge()+",weight:"+getWeight();
	}
	public static Pet createFromData(String data) {
		String[] args = data.split(",");
		String _name = args[0].split("\\:")[1];
		int _age = Integer.parseInt(args[1].split("\\:")[1]);
		double _weight = Double.parseDouble(args[2].split("\\:")[1]);
		return new Pet(_name, _age, _weight);
	}
}