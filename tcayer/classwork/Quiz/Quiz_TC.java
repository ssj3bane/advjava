package tcayer.classwork.Quiz;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.function.Consumer;
import java.util.regex.Pattern;

import tcayer.improvements.Print;
import tcayer.improvements.Prompt;

public class Quiz_TC {
	private static ArrayList<Boolean> Answers = new ArrayList<Boolean>();
	private static ArrayList<String> Questions = new ArrayList<String>();
	/*
	 * Quiz reader format
	 * answer|question
	 * 
	 */
	public static void main(String[] args) throws FileNotFoundException, ClassNotFoundException {
        Scanner inputStream = new Scanner(new FileInputStream("qa.txt"));
        while(inputStream.hasNextLine()) {
        	String line = inputStream.nextLine();
        	String[] sepLine = line.split("\\|");
        	Answers.add(sepLine[0].equals("true")?true:false);
        	Questions.add(sepLine[1]);
        }
        inputStream.close();
        // Above is basic file reader
        Quiz myQuiz = new Quiz(Questions, Answers);
        Questions.forEach(myQuiz);
        Print.ln(myQuiz.calculate() * 100);
    }
}
class Quiz implements Consumer<Object> {
	private Scanner myInput = new Scanner(System.in);
	private ArrayList<Boolean> rightAnswers = new ArrayList<Boolean>();
	private ArrayList<String> questions;
	private ArrayList<Boolean> answers;
	public Quiz(ArrayList<String> _questions, ArrayList<Boolean> _answers) {
		questions = _questions;
		answers = _answers;
	}
	@Override
	public void accept(Object arg0) {
		Boolean answer = answers.get(questions.indexOf(arg0));
		Boolean myAnswer = null;
		Print.p(arg0 + ": ");
		while(myAnswer == null)
			if(myInput.hasNextBoolean())
				myAnswer = myInput.nextBoolean();
			else
				myInput.next();
		rightAnswers.add((answer == myAnswer));
	}
	public float calculate() {
		float percent = 0;
		for(Boolean answer : rightAnswers.toArray(new Boolean[0])) {
			percent += answer ? 1 : 0;
		}
		return percent / rightAnswers.size();
	}
	public void close() {
		myInput.close();
	}
}