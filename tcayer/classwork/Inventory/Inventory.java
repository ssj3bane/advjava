package tcayer.classwork.Inventory;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Inventory {
	public static HashMap<Integer, double[]> INVENTORY = new HashMap<Integer, double[]>();
	public static void main(String[] args) throws FileNotFoundException, ClassNotFoundException {
		Scanner input = new Scanner(System.in);
		int choice = 0;
		getInventory();
        while(choice != 3) {
			choice = (int) Prompt.prompt("ENTRY MUST BE AN INTEGER\n1: Calculate total inventory value\n2: Get a specific item by number\n3: Quit\n", input, new Integer(1));
        	switch(choice) {
        		case 0:
	        		break;
	        	case 1:
	    			calculateInventory();
	    			break;
	        	case 2:
        			getItem((int) Prompt.prompt("ENTRY MUST BE AN INTEGER\nWhat item would you like to look at?\n", input, new Integer(1)));
	    			break;
	        	case 3:
	                input.close();
	                System.exit(0);
	    			break;
	        	default:
	    			break;
        	}
        }
        input.close();
        
	}
	public static void getInventory() throws FileNotFoundException, ClassNotFoundException {
		Scanner inputStream = new Scanner(new FileInputStream("Inventory.txt"));
		while(inputStream.hasNextLine()) {
			String line = inputStream.nextLine();
			String[] item = line.split(" ");
			int itemNum = Integer.parseInt(item[0]);
			double[] itemData = new double[] {
				Double.parseDouble(item[1]),
				Double.parseDouble(item[2])
			};
			INVENTORY.put(itemNum, itemData);
		}
		inputStream.close();
	}
	public static void calculateInventory() {
		double totalCost = 0;
		for(Integer key : INVENTORY.keySet()) {
			double[] itemData = INVENTORY.get(key);
			int quantity = ((int) itemData[0]);
			double itemCost = itemData[1];
			totalCost += quantity * itemCost;
		}
		Print.ln("Total: $" + totalCost);
	}
	
	public static void getItem(int itemNum) {
		double[] itemData = INVENTORY.get(itemNum);
		Print.ln("Quantity: " + ((int) itemData[0]) + ", Cost: $" + itemData[1]);
	}
}

class Print {
	/**
	 * Print a new line of arg0
	 * @param arg0
	 */
	public static void ln(Object arg0) {
		System.out.println(arg0);
	}
	/**
	 * Print a new line of arg0
	 * @param arg0
	 */
	public static void ln(Object[] arg0) {
		System.out.println(arg0);
	}
	/**
	 * Print arg0
	 * @param arg0
	 */
	public static void p(Object arg0) {
		System.out.print(arg0);
	}
	/**
	 * Print arg0
	 * @param arg0
	 */
	public static void p(Object[] arg0) {
		System.out.print(arg0);
	}
}

class Prompt {
	/**
	 * A simple prompt. Ask and ye shall receive.
	 * @param prompt - String to send to System.out
	 * @param input - Scanner to get .next...() from
	 * @param type - Any Object, output will be same as the type
	 * @return Result from input respective to the inputed type
	 */
	public static Object prompt(String prompt, Scanner input, Object type) throws InputMismatchException {
		Print.p(prompt);
		Object rv = new Error();
		boolean t = true;
		if(type instanceof Integer)
			if(input.hasNextInt())
				rv = input.nextInt();
		else if(type instanceof String)
			if(((String)type).toLowerCase().equals("line"))
				if(input.hasNextLine())
					rv = input.nextLine();
			else
				if(input.hasNext())
					rv = input.next();
		else if(type instanceof Boolean)
			if(input.hasNextBoolean())
				rv = input.nextBoolean();
		else if(type instanceof Float)
			if(input.hasNextFloat())
				rv = input.nextFloat();
		else if(type instanceof Double)
			if(input.hasNextDouble())
				rv = input.nextDouble();
		while(rv instanceof Error && t) {
			if(type instanceof Integer)
				rv = input.nextInt();
			else if(type instanceof String)
				rv = input.next();
			else if(type instanceof Boolean)
				rv = input.nextBoolean();
			else if(type instanceof Float)
				rv = input.nextFloat();
			else if(type instanceof Double)
				rv = input.nextDouble();
			t = false;
		}
		return rv;
	}
}