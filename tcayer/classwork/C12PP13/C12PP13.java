package tcayer.classwork.C12PP13;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import tcayer.improvements.Print;

public class C12PP13 {
	public static LinkedList<String> test = new LinkedList<String>();
	public static void main(String[] args) throws FileNotFoundException {
		Scanner surveyFile = new Scanner(new FileInputStream("BirdSpecies.txt"));
		BirdSurvey newSurvey = new BirdSurvey(surveyFile);
		newSurvey.getReport();
	}
}
class BirdSurvey {
	private HashMap<String, Integer> internalList = new HashMap<String, Integer>();
	public BirdSurvey() {
			
	}
	public BirdSurvey(Scanner file) {
		while(file.hasNextLine()) {
        	String species = file.nextLine();
        	add(species);
        }
        file.close();
	}
	public void add(String birb) {
		if(internalList.containsKey(birb)) {
			internalList.put(birb, internalList.get(birb).intValue() + 1);
		} else {
			internalList.put(birb, 1);
		}
	}
	public int getCount(String birb) {
		return internalList.containsKey(birb)?internalList.get(birb):-1;
	}
	
	public void getReport() {
		internalList.forEach(new BirbReporter());
	}
	
	class BirbReporter implements BiConsumer<String, Integer> {
		@Override
		public void accept(String arg0, Integer arg1) {
			System.out.println("Species: " + arg0 + ", Count: " + arg1);
		}
	}
}