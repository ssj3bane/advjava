package tcayer.classwork.BinarySearch;

import tcayer.improvements.Print;

public class BinarySearch {
	public static int[] searchedArray = new int[] {
		1,2,3,4,5,6,7,8,9,10,20,30,40,50,55,70,72,75,77,80,90,100
	};
	public static void main(String[] args) {
		int valueToFind = 70;
		int result = binarySearch(searchedArray, 0, searchedArray.length - 1, valueToFind, 0);
		Print.ln("searchedArray[" + result + "] = " + searchedArray[result]);
		int[] newArray = buildArray(new int[20]);
		valueToFind = 2;
		result = binarySearch(newArray, 0, newArray.length - 1, valueToFind, 0);
		Print.ln("newArray[" + result + "] = " + newArray[result]);
		result = binarySearch(newArray, 0, newArray.length - 1, valueToFind);
		Print.ln("newArray[" + result + "] = " + newArray[result]);
	}
	/*
	 * Efficient method
	 */
	public static int binarySearch(int[] arr, int beginIndex, int endIndex, int searchValue, int count) {
		if(endIndex >= beginIndex) {
			++count;
			int mid = beginIndex + (endIndex - beginIndex) / 2;
			Print.ln((count!=1?"--------------------\n":"") + "beginIndex: " + beginIndex + "\nendIndex: " + endIndex + "\nsearchValue: " + searchValue + "\nmid: " + mid);
			if(arr[mid] == searchValue) {
				Print.ln("count: " + count);
				return mid;
			} else if(arr[mid] > searchValue)
				return binarySearch(arr, beginIndex, mid, searchValue, count);
			else if(arr[mid] < searchValue)
				return binarySearch(arr, mid, endIndex, searchValue, count);
		}
		return -1;
	}
	/*
	 * Intentional bad method
	 */
	public static int binarySearch(int[] arr, int beginIndex, int endIndex, int searchValue) {
		int count = 0;
		int mid = 0;
		while(endIndex >= beginIndex) {
			mid = beginIndex + (endIndex - beginIndex) / 2;
			Print.ln((count!=1?"--------------------\n":"") + "beginIndex: " + beginIndex + "\nendIndex: " + endIndex + "\nsearchValue: " + searchValue + "\nmid: " + mid);
			if(arr[mid] == searchValue) {
				Print.ln("count: " + count);
				return mid;
			} else if(arr[mid] > searchValue)
				endIndex = mid;
			else if(arr[mid] < searchValue)
				beginIndex = mid;
		}
		return -1;
	}
	public static int[] buildArray(int[] arr) {
		for(int i = 0; i < arr.length; i++)
			arr[i] = i;
		return arr;
	}
}
