package tcayer.classwork.Reservations;

public class Room {
	private int roomNumber;
	private int capacity;
	private boolean smart;
	private boolean occupied;
	public Room() {
		setRoomNumber(0);
		setCapacity(0);
		setSmart(false);
		setOccupied(false);
	}
	public Room(int roomNumber, int capacity, boolean smart, boolean occupied) {
		setRoomNumber(roomNumber);
		setCapacity(capacity);
		setSmart(smart);
		setOccupied(occupied);
	}
	public boolean isOccupied() {
		return occupied;
	}
	public void setOccupied(boolean occupied) {
		this.occupied = occupied;
	}
	public boolean isSmart() {
		return smart;
	}
	public void setSmart(boolean smart) {
		this.smart = smart;
	}
	public int getCapacity() {
		return capacity;
	}
	public void setCapacity(int capacity) {
		if(capacity >= 20 && capacity <= 50) {
			this.capacity = capacity;
		}
	}
	public int getRoomNumber() {
		return roomNumber;
	}
	public void setRoomNumber(int roomNumber) {
		if(roomNumber >= 1 && roomNumber <= 24) {
			this.roomNumber = roomNumber;
		}
	}
	public String toString() {
		return "Room number: "+getRoomNumber()+"\n\tCapacity: "+getCapacity()+"\n\tSmart: "+isSmart()+"\n\tOccupied: "+isOccupied();
	}
	public boolean equals(Room room) {
		if(	room.getCapacity() == getCapacity() &&
			room.getRoomNumber() == getRoomNumber() &&
			room.isSmart() == isSmart() &&
			room.isOccupied() == isOccupied()) {
			return true;
		} else {
			return false;
		}
	}
}
