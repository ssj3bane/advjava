package tcayer.classwork.Reservations;

import java.util.Random;
import java.util.Scanner;

/*
 * I made a library for basic improvements that java.util doesn't provide
 * I've appended the two classes, used for Prompt, to end the.
 */
//import tcayer.improvements.Prompt;

public class RoomReservations {
	public static final String selectionMenu= "1. List all rooms.\n2. List all available rooms.\n3. List all available smart rooms.\n4. List all available non-smart rooms.\n5. List all available rooms sorted by capacity (low to high).\n6. List all available smart rooms sorted by capacity.\n7. List all available non-smart rooms sorted by capacity (low to high).\n8. Reserve a room.\n9. Cancel a room reservation.\n0. Quit.\nChoice: ";
	public static Random rnd = new Random();
	public static Room[] reservations = new Room[24];
	public static void main(String[] args) {
		setupReservations();
		select();
	}
	public static void select() {
		Scanner kb = new Scanner(System.in);
		int choice = (int) Prompt.prompt(selectionMenu, kb, 1);
		System.out.println(choice);
		switch(choice) {
			case 1:
				listRooms();
				break;
			case 2:
				listAvailableRooms();
				break;
			case 3:
				listAvailableSmartRooms();
				break;
			case 4:
				listAvailableNonSmartRooms();
				break;
			case 5:
				listAvailableRoomsSortedByCapacity();
				break;
			case 6:
				listAvailableSmartRoomsSortedByCapacity();
				break;
			case 7:
				listAvailableNonSmartRoomsSortedByCapacity();
				break;
			case 8:
				reserveRoom((int) Prompt.prompt("Reserve a room in room number: ", kb, 1));
				break;
			case 9:
				cancelRoomReservation((int) Prompt.prompt("Cancel a reservation in room: ", kb, 1));
				break;
			case 0:
				System.exit(0);
				break;
		}
		select();
	}
	public static void setupReservations() {
		for(int i = 0; i < 24; i++) {
			reservations[i] = new Room(i+1, rnd.nextInt(31)+20, rnd.nextBoolean(), false);
		}
	}
	/*
	 * This entire section is gonna be really bad in terms of reusing methods...
	 */
	public static void bubbleSortReservations(String prop) {
		for(int i = 0; i < reservations.length-1; i++) {
			for(int j = 0; j < reservations.length-i-1; j++) {
				if(prop.equals("roomNumber")) {
					if(reservations[j].getRoomNumber() > reservations[j+1].getRoomNumber()) {
						Room temp = reservations[j];
						reservations[j] = reservations[j+1];
						reservations[j+1] = temp;
					}
				} else if(prop.equals("capacity")) {
					if(reservations[j].getCapacity() > reservations[j+1].getCapacity()) {
						Room temp = reservations[j];
						reservations[j] = reservations[j+1];
						reservations[j+1] = temp;
					}
				}
			}
		}
	}
	public static void listRooms() {
		bubbleSortReservations("roomNumber");
		for(Room room : reservations) {
			System.out.println(room.toString());
		}
	}
	public static void listAvailableRooms() {
		bubbleSortReservations("roomNumber");
		for(Room room : reservations) {
			if(!room.isOccupied()) {
				System.out.println(room.toString());
			}
		}
	}
	public static void listAvailableSmartRooms() {
		bubbleSortReservations("roomNumber");
		for(Room room : reservations) {
			if(!room.isOccupied() && room.isSmart()) {
				System.out.println(room.toString());
			}
		}
	}
	public static void listAvailableNonSmartRooms() {
		bubbleSortReservations("roomNumber");
		for(Room room : reservations) {
			if(!room.isOccupied() && !room.isSmart()) {
				System.out.println(room.toString());
			}
		}
	}
	public static void listAvailableRoomsSortedByCapacity() {
		bubbleSortReservations("capacity");
		for(Room room : reservations) {
			if(!room.isOccupied()) {
				System.out.println(room.toString());
			}
		}
	}
	public static void listAvailableSmartRoomsSortedByCapacity() {
		bubbleSortReservations("capacity");
		for(Room room : reservations) {
			if(!room.isOccupied() && room.isSmart()) {
				System.out.println(room.toString());
			}
		}
	}
	public static void listAvailableNonSmartRoomsSortedByCapacity() {
		bubbleSortReservations("capacity");
		for(Room room : reservations) {
			if(!room.isOccupied() && !room.isSmart()) {
				System.out.println(room.toString());
			}
		}
	}
	public static void reserveRoom(int roomNumber) {
		if(roomNumber > 0 && roomNumber <= reservations.length) {
			if(!reservations[roomNumber-1].isOccupied()) {
				reservations[roomNumber-1].setOccupied(true);
			} else {
				throw new Error("Room is already reserved");
			}
		} else {
			throw new Error("Room does not exist");
		}
	}
	public static void cancelRoomReservation(int roomNumber) {
		if(roomNumber > 0 && roomNumber <= reservations.length) {
			if(reservations[roomNumber-1].isOccupied()) {
				reservations[roomNumber-1].setOccupied(false);
			} else {
				throw new Error("Room was not reserved");
			}
		} else {
			throw new Error("Room does not exist");
		}
	}

}

class Prompt {
	public static Object prompt(String _prompt, Scanner input, Object type) {
		Print.p(_prompt);
		Object rv = new Error();
		boolean t = true;
		if(type instanceof Integer)
			if(input.hasNextInt())
				rv = input.nextInt();
		else if(type instanceof String)
			if(input.hasNext())
				rv = input.next();
		else if(type instanceof Boolean)
			if(input.hasNextBoolean())
				rv = input.nextBoolean();
		else if(type instanceof Float)
			if(input.hasNextFloat())
				rv = input.nextFloat();
		else if(type instanceof Double)
			if(input.hasNextDouble())
				rv = input.nextDouble();
		while(rv instanceof Error && t) {
			if(type instanceof Integer)
				rv = input.nextInt();
			else if(type instanceof String)
				rv = input.next();
			else if(type instanceof Boolean)
				rv = input.nextBoolean();
			else if(type instanceof Float)
				rv = input.nextFloat();
			else if(type instanceof Double)
				rv = input.nextDouble();
			t = false;
		}
		return rv;
	}
}

class Print {
	public static void ln(Object arg0) {
		System.out.println(arg0);
	}
	public static void ln(Object[] arg0) {
		System.out.println(arg0);
	}
	public static void p(Object arg0) {
		System.out.print(arg0);
	}
	public static void p(Object[] arg0) {
		System.out.print(arg0);
	}
}
