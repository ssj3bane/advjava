package tcayer.classwork.Helping;

import java.util.LinkedList;
import java.util.function.Consumer;

public class Helping {
	public static void main(String[] args) {
		BirdSurvey mySurvey = new BirdSurvey();
		mySurvey.addBird(new Bird("Robin"));

		mySurvey.addBird(new Bird("Swallow"));

		mySurvey.addBird(new Bird("Robin"));

		mySurvey.addBird(new Bird("Swallow"));
		
		mySurvey.addBird(new Bird("Bluebird"));
		
		mySurvey.getReport();
	}
}
class BirdSurvey {
	public LinkedList<Bird> birdList = new LinkedList<Bird>();
	public Boolean externalBool = false;
	public BirdSurvey() {
		
	}
	public void addBird(Bird bird) {
		externalBool = false;
		birdList.forEach(new BirdExistsConsumer(bird, this));
		if(!externalBool) {
			if(birdList.size() == 0) {
				birdList.add(bird);
			} else {
				addBird(birdList.getLast(), bird);
			}
		} else {
			birdList.forEach(new BirdCountConsumer(bird));
		}
		externalBool = false;
	}
	public void addBird(Bird bird1, Bird bird2) {
		if(!birdList.contains(bird1)) {
			birdList.add(bird1);
		}
		bird1.linkBird(bird2);
		birdList.add(bird2);
	}
	public int getCount(Bird bird) {
		return bird.count;
	}
	public void getReport() {
		birdList.forEach(new BirdConsumer());
	}
}
class Bird {
	public String species;
	public int count = 1;
	public Bird next = null;
	public Bird() {
		this.species = null;
	}
	public Bird(String species) {
		this.species = species;
		this.count = 1;
	}
	public Bird(String species, int count, Bird next) {
		this.species = species;
		this.count = count;
		this.next = next;
	}
	public void linkBird(Bird nextBird) {
		this.next = nextBird;
	}
	
	public String toString() {
		return "Species: " + species + ", Count: " + count;
	}
}
class BirdConsumer implements Consumer<Bird> {
	@Override
	public void accept(Bird t) {
		System.out.println(t.toString());
	}
}
class BirdExistsConsumer implements Consumer<Bird> {
	private Bird compareBird;
	private BirdSurvey tempSurvey;
	public BirdExistsConsumer(Bird c, BirdSurvey tempSurvey) {
		compareBird = c;
		this.tempSurvey = tempSurvey;
	}
	@Override
	public void accept(Bird t) {
		if(t.species.equals(compareBird.species)) {
			tempSurvey.externalBool = true;
		}
	}
}
class BirdCountConsumer implements Consumer<Bird> {
	private Bird compareBird;
	public BirdCountConsumer(Bird c) {
		compareBird = c;
	}
	@Override
	public void accept(Bird t) {
		if(t.species.equals(compareBird.species))
			t.count++;
	}
}