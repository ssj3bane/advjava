package tcayer.classwork.Recursion;

import java.util.Scanner;

import tcayer.improvements.Print;
import tcayer.improvements.Prompt;

public class Recursion {
	public static Long time = new Long(0);
	public static void main(String[] args) {
		Prompt myPrompt = new Prompt();
//		Print.ln(ExtendedMath.factorial((long) myPrompt.prompt("Factorial ", new Long("1"))));
//		Print.ln(YeOldeAlgorithms.totalGrains((int) myPrompt.prompt("Rice Squares ", new Integer(1)), (int) myPrompt.prompt("Rice Grains ", new Integer(1))));
//		Print.ln(YeOldeAlgorithms.fibonacci((int) myPrompt.prompt("Fibonacci ", new Integer(1))));
		Print.ln(YeOldeAlgorithms.hanoi((int) myPrompt.prompt("Tower of Hanoi Discs ", new Integer(1))));
		myPrompt.close();
	}

	static class ExtendedMath {
		public static long factorial(long n) {
			return n > 1 ? n *= factorial(n - 1) : n;
		}
	}
	
	static class YeOldeAlgorithms {
		public static long totalGrains(int squares, long grains) {
			return squares > 1 ? grains += totalGrains(squares - 1, grains * 2) : grains;
		}
	
		public static int fibonacci(int index) {
			return index > 2 ? fibonacci(index - 2) + fibonacci(index - 1) : 1;
		}
		public static long hanoi(int disc) {
			extendedHanoi(disc, 'A', 'B', 'C');
			return (long) (Math.pow(2, disc) - 1);
		}
		public static void extendedHanoi(int disc, char from, char to, char aux) {
			if(disc == 1) {
				Print.ln("Moving 1 from " + from + " to " + to);
				return;
			} else {
				extendedHanoi(disc - 1, from, aux, to);
				Print.ln("Moving " + disc + " from " + from + " to " + to);
				extendedHanoi(disc - 1, aux, to, from);
			}
		}
	}
}