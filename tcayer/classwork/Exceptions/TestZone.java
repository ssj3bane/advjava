package tcayer.classwork.Exceptions;

import java.util.Scanner;

import tcayer.improvements.Print;
import tcayer.improvements.Prompt;
import tcayer.improvements.Regex;
public class TestZone {
	public static void main(String[] args) {
		// Scanner
		Scanner input = new Scanner(System.in);
		// Password
		String text = "";
		// Global passing flag
		Boolean validPassword = false;
		while(!validPassword) {
			// Method written to simplify prompts, equivalent to input.nextLine()
			text = (String) Prompt.prompt("Enter a password: ", input, "1");
			// Check for length of 8>
			Boolean lengthFlag		= text.length() >= 8;
			// Check for uppercase
			Boolean uppercaseFlag	= Regex.match("([A-Z]+)", text);
			// Check for lowercase
			Boolean lowercaseFlag	= Regex.match("([a-z]+)", text);
			// Check for digits
			Boolean digitFlag		= Regex.match("([0-9]+)", text);
			// Check for special characters
			Boolean charFlag		= Regex.match("([\\!\\#\\$\\%\\*\\-\\_]+)", text);
			// Make sure every flag is true
			validPassword			=	lengthFlag &&
										uppercaseFlag &&
										lowercaseFlag &&
										digitFlag &&
										charFlag;
			if(!validPassword) {
				Print.ln("Invalid password");
			}			
		}
		Print.ln("Valid password: " + text);
	}
}