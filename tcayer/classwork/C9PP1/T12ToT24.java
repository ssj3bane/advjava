package tcayer.classwork.C9PP1;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.regex.Pattern;

public class T12ToT24{
	public static void main(String[] args) throws TimeFormatException {
		Scanner input = new Scanner(System.in);
		String response = "y";
		while(response.toLowerCase() != "n") {
			String time = (String) Prompt.prompt("Enter time in 24-hour notation:\n", input, "1");
			SimpleDateFormat myDate = new SimpleDateFormat("hh:mm");
			Date date = new Date();
			try {
				if(!Regex.match("([a-zA-Z]+)", time)) {
					String[] separatedTime = time.split(":");
					if(!(	Integer.parseInt(separatedTime[1]) > 60 || Integer.parseInt(separatedTime[1]) < 0 ||	// Minute
							Integer.parseInt(separatedTime[0]) > 24 || Integer.parseInt(separatedTime[0]) < 0)) {	// Hours
						date = myDate.parse(time);
					} else {
						throw(new ParseException("Hour and/or minute is out of acceptable range", 0));
					}
				} else {
					throw(new ParseException("Cannot use alphabetic characters in date", 0));
				}
			} catch (ParseException e) {
				throw(new TimeFormatException(e.getMessage(),e.getErrorOffset()));
			}
			myDate.applyPattern("hh:mm a");
			Print.ln(myDate.format(date));
			response = (String) Prompt.prompt("Again? (y/n)", input, "1");
		}
	}
	
}

class TimeFormatException extends ParseException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2100940279324592607L;
	public TimeFormatException(String arg0, int arg1) {
		super(arg0, arg1);
	}
	public TimeFormatException(int arg1) {
		super("Time Format Exception", arg1);
	}
}

class Regex {
	public static Boolean match(String regex, String matchedText) {
		return Pattern.compile(regex).matcher(matchedText).find();
	}
}

class Print {
	/**
	 * Print a new line of arg0
	 * @param arg0
	 */
	public static void ln(Object arg0) {
		System.out.println(arg0);
	}
	/**
	 * Print a new line of arg0
	 * @param arg0
	 */
	public static void ln(Object[] arg0) {
		System.out.println(arg0);
	}
	/**
	 * Print arg0
	 * @param arg0
	 */
	public static void p(Object arg0) {
		System.out.print(arg0);
	}
	/**
	 * Print arg0
	 * @param arg0
	 */
	public static void p(Object[] arg0) {
		System.out.print(arg0);
	}
}

class Prompt {
	/**
	 * A simple prompt. Ask and ye shall receive.
	 * @param prompt - String to send to System.out
	 * @param input - Scanner to get .next...() from
	 * @param type - Any Object, output will be same as the type
	 * @return Result from input respective to the inputed type
	 */
	public static Object prompt(String prompt, Scanner input, Object type) {
		Print.p(prompt);
		Object rv = new Error();
		boolean t = true;
		if(type instanceof Integer)
			if(input.hasNextInt())
				rv = input.nextInt();
		else if(type instanceof String)
			if(((String)type).toLowerCase().equals("line"))
				if(input.hasNextLine())
					rv = input.nextLine();
			else
				if(input.hasNext())
					rv = input.next();
		else if(type instanceof Boolean)
			if(input.hasNextBoolean())
				rv = input.nextBoolean();
		else if(type instanceof Float)
			if(input.hasNextFloat())
				rv = input.nextFloat();
		else if(type instanceof Double)
			if(input.hasNextDouble())
				rv = input.nextDouble();
		while(rv instanceof Error && t) {
			if(type instanceof Integer)
				rv = input.nextInt();
			else if(type instanceof String)
				rv = input.next();
			else if(type instanceof Boolean)
				rv = input.nextBoolean();
			else if(type instanceof Float)
				rv = input.nextFloat();
			else if(type instanceof Double)
				rv = input.nextDouble();
			t = false;
		}
		return rv;
	}
}