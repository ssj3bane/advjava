package tcayer.classwork.FileHunting;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import tcayer.improvements.*;

public class FileHunting {
	public static HashMap<String, Class> abc;
	public static void main(String[] args) {
		abc.put("MyClass", (Class) Regex.class);
		Print.ln(abc.get("MyClass"));
		int numberRead;
		int numberTotal = 0;
		int numberCounted = 0;
		Scanner inputStream = null;
		try {
			inputStream = new Scanner(new FileInputStream("C:/Users/timom/Desktop/CodingResources/numbers.txt"));
		}
		catch(FileNotFoundException err) {
			ErrPrint.ln("Message: " + err.getMessage());
			err.printStackTrace();
			System.exit(0);
		}
		while(inputStream.hasNextInt()) {
			numberRead = inputStream.nextInt();
			numberTotal += numberRead;
			numberCounted++;
			Print.ln(numberRead);
		}
		Print.ln("Total: " + numberTotal + ", Average: " + (numberTotal / numberCounted));
	}
}
