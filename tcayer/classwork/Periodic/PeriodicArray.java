package tcayer.classwork.Periodic;

import tcayer.improvements.Print;

public class PeriodicArray {
	public static Periodic[] elementArray = new Periodic[] {
		new PeriodicNonMetal("H", 1, 1.008),
		new PeriodicNonMetal("He", 2, 4.0026022),
		new PeriodicMetal("Li", 3, 6.94),
		new PeriodicMetal("Be", 4, 9.01218315),
		new PeriodicMetal("B", 5, 10.81),
		new PeriodicNonMetal("C", 6, 12.011),
			
	};
	public static void main(String[] args) {
		for(Periodic element : elementArray) {
			Print.ln(element.toString());
			element.describeElement();
		}
	}
}
