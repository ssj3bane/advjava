package tcayer.classwork.Periodic;

public abstract class Periodic {
	private String symbol;
	private int atomicNumber;
	private double atomicWeight;
	public Periodic() {
		setSymbol("");
		setAtomicNumber(0);
		setAtomicWeight(0);
	}
	public Periodic(String symbol, int atomicNumber, double atomicWeight) {
		setSymbol(symbol);
		setAtomicNumber(atomicNumber);
		setAtomicWeight(atomicWeight);
	}
	public String getSymbol() {
		return symbol;
	}
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	public int getAtomicNumber() {
		return atomicNumber;
	}
	public void setAtomicNumber(int atomicNumber) {
		this.atomicNumber = atomicNumber;
	}
	public double getAtomicWeight() {
		return atomicWeight;
	}
	public void setAtomicWeight(double atomicWeight) {
		this.atomicWeight = atomicWeight;
	}
	public String toString() {
		return	"Symbol: " + getSymbol() +
				"\nAtomic Number: " + getAtomicNumber() +
				"\nAtomic  Weight: " + getAtomicWeight();
	}
	public boolean equals(Periodic compareTo) {
		return ((getSymbol().equals(compareTo.getSymbol())) &&
				(getAtomicNumber() == compareTo.getAtomicNumber()) &&
				(getAtomicWeight() == compareTo.getAtomicWeight()));
	}
	public void describeElement() {
		
	}
}
