package tcayer.classwork.Periodic;

import tcayer.improvements.Print;

public class PeriodicMetal extends Periodic {
	public PeriodicMetal() {
		super("", 0, 0);
	}
	public PeriodicMetal(String symbol, int atomicNumber, double atomicWeight) {
		super(symbol, atomicNumber, atomicWeight);
	}
	public void describeElement() {
		Print.ln("Metals are good conductors");
	}
}
