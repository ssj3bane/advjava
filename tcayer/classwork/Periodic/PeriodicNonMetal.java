package tcayer.classwork.Periodic;

import tcayer.improvements.Print;

public class PeriodicNonMetal extends Periodic {
	public PeriodicNonMetal() {
		super("", 0, 0);
	}
	public PeriodicNonMetal(String symbol, int atomicNumber, double atomicWeight) {
		super(symbol, atomicNumber, atomicWeight);
	}
	public void describeElement() {
		Print.ln("Non-metals are not good conductors");
	}
}
