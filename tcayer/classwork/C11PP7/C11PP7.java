package tcayer.classwork.C11PP7;

public class C11PP7 {
	private static void recurse(String[][] data, String input, int row, int col) {
		String output = input + data[row][col];
		if (row == data.length - 1)
			System.out.println(output);
		if (row < data.length - 1)
			recurse(data, output, row + 1, 0);
		if (col < data[row].length - 1)
			recurse(data, input, row, col + 1);
	}

	public static void main(String[] args) {
		String[][] data1 = { { "A", "B" }, { "1", "2" }, { "XX", "YY", "ZZ" } };
		String[][] data2 = { { "A" }, { "1" }, { "2" }, { "XX", "YY" } };
		System.out.println("Data 1:");
		recurse(data1, "", 0, 0);
		System.out.println("\nData 2:");
		recurse(data2, "", 0, 0);
	}
}