package tcayer.classwork.Aristotle;

public class Tile {
	private int value = 0;
	private boolean placed = false;
	public Tile(int value) {
		setValue(value);
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public boolean isPlaced() {
		return placed;
	}
	public void setPlaced(boolean placed) {
		this.placed = placed;
	}
	public void setTile(Tile newTile) {
		setValue(newTile.getValue());
		setPlaced(newTile.isPlaced());
	}
	public String toString() {
		return "" + getValue();
	}
}
