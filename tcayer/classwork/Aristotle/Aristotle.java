package tcayer.classwork.Aristotle;

import java.util.Scanner;

public class Aristotle {
	public static Scanner input;
	public static Tile[] tiles = new Tile[19];
	public static final Object[] puzzle = new Object[] {
		new Tile[3],
		new Tile[4],
		new Tile[5],
		new Tile[4],
		new Tile[3]
	};
	public static String lastCommand = "";
	public static void main(String[] args) {
		setup();
		printBoard();
		help();
		trueMain();
	}
	public static void setup() {
		input = new Scanner(System.in);
		for(int i = 0; i < tiles.length; i++) {
			tiles[i] = new Tile(i+1);
		}

		for(int i = 0; i < puzzle.length; i++) {
			for(int j = 0; j < ((Object[]) puzzle[i]).length; j++) {
				((Object[])puzzle[i])[j] = new Tile(0);
			}
		}
	}
	public static void place(int row, int column, Tile tile) {
		if(column >= 0 && column < puzzle.length) {
			if(row >= 0 && row < ((Object[]) puzzle[column]).length) {
				if(!tile.isPlaced()) {
					tile.setPlaced(true);
					((Tile[]) puzzle[column])[row].setTile(tile);
				}
			}
		}
	}
	public static void remove(int row, int column, Tile tile) {
		if(column >= 0 && column < puzzle.length) {
			if(row >= 0 && row < ((Object[]) puzzle[column]).length) {
				if(tile.isPlaced()) {
					tile.setPlaced(false);
					((Tile[]) puzzle[column])[row].setTile(new Tile(0));
				}
			}
		}
	}
	public static void printBoard() {
		for(Object row : puzzle) {
			for(int i = 0; i < (5-((Object[])row).length); i ++) {
				Print.p(" ");
			}
			for(Tile tile : (Tile[]) row) {
				Print.p(tile.toString() + " ");
			}
			Print.ln("");
		}
		Print.ln("");
	}
	public static void printAvailableTiles() {
		Print.ln("Tiles: ");
		for(Tile tile : tiles) {
			Print.ln("Value: "  + tile.toString() + "\tPlaced: " + tile.isPlaced());
		}
	}
	public static void parseString(String sentence) {
		String[] words = sentence.split("_");
		lastCommand = sentence;
		switch(words[0].toLowerCase()) { 
			case "view":
				if(words.length == 2) {
					if(words[1].toLowerCase().equals("board")) {
						printBoard();
					} else if(words[1].toLowerCase().equals("tiles")) {
						printAvailableTiles();
					}
				}
				break;
			case "help":
				help();
				break;
			case "place":
				if(words.length == 5) {
					int tileNumber = Integer.parseInt(words[1])-1;
					int rowNumber = Integer.parseInt(words[3])-1;
					int columnNumber = Integer.parseInt(words[4])-1;
					place(rowNumber, columnNumber, tiles[tileNumber]);
				}
				break;
			case "remove":
				if(words.length == 5) {
					int tileNumber = Integer.parseInt(words[1])-1;
					int rowNumber = Integer.parseInt(words[3])-1;
					int columnNumber = Integer.parseInt(words[4])-1;
					remove(rowNumber, columnNumber, tiles[tileNumber]);
				}
				break;
			case "check":
				checkPuzzle();
				break;
		}
	}
	public static void checkPuzzle() {
		/*
		 * I can't be bothered to write an algorithm that sums a hexagonal array
		 */
		/*	Sum patterns
		 * 	puzzle[0][0] + puzzle[0][1] + puzzle[0][2]
		 * 	puzzle[1][0] + puzzle[1][1] + puzzle[1][2] + puzzle[1][3]
		 * 	puzzle[2][0] + puzzle[2][1] + puzzle[2][2] + puzzle[2][3] + puzzle[2][4]
		 * 	puzzle[3][0] + puzzle[3][1] + puzzle[3][2] + puzzle[3][3]
		 * 	puzzle[4][0] + puzzle[4][1] + puzzle[4][2]
		 *	puzzle[0][0] + puzzle[1][0] + puzzle[2][0]
		 *	puzzle[0][1] + puzzle[1][1] + puzzle[2][1] + puzzle[3][0]
		 *	puzzle[0][2] + puzzle[1][2] + puzzle[2][2] + puzzle[3][1] + puzzle[4][0]
		 *	puzzle[1][3] + puzzle[2][3] + puzzle[3][2] + puzzle[4][1]
		 *	puzzle[2][4] + puzzle[3][3] + puzzle[4][2]
		 *	puzzle[2][0] + puzzle[3][0] + puzzle[4][0]
		 *	puzzle[1][0] + puzzle[2][1] + puzzle[3][1] + puzzle[4][1]
		 *	puzzle[0][0] + puzzle[1][1] + puzzle[2][2] + puzzle[3][2] + puzzle[4][2]
		 *	puzzle[0][1] + puzzle[1][2] + puzzle[2][3] + puzzle[3][3]
		 *	puzzle[0][2] + puzzle[1][3] + puzzle[3][4]
		 */
		int[] puzzleSums = new int[] {
			(((Tile[]) puzzle[0])[0].getValue()) + (((Tile[]) puzzle[0])[1].getValue()) + (((Tile[]) puzzle[0])[2].getValue()),
			(((Tile[]) puzzle[1])[0].getValue()) + (((Tile[]) puzzle[1])[1].getValue()) + (((Tile[]) puzzle[1])[2].getValue()) + (((Tile[]) puzzle[1])[3].getValue()),
			(((Tile[]) puzzle[2])[0].getValue()) + (((Tile[]) puzzle[2])[1].getValue()) + (((Tile[]) puzzle[2])[2].getValue()) + (((Tile[]) puzzle[2])[3].getValue()) + (((Tile[]) puzzle[2])[4].getValue()),
			(((Tile[]) puzzle[3])[0].getValue()) + (((Tile[]) puzzle[3])[1].getValue()) + (((Tile[]) puzzle[3])[2].getValue()) + (((Tile[]) puzzle[3])[3].getValue()),
			(((Tile[]) puzzle[4])[0].getValue()) + (((Tile[]) puzzle[4])[1].getValue()) + (((Tile[]) puzzle[4])[2].getValue()),
			(((Tile[]) puzzle[0])[0].getValue()) + (((Tile[]) puzzle[1])[0].getValue()) + (((Tile[]) puzzle[2])[0].getValue()),
			(((Tile[]) puzzle[0])[1].getValue()) + (((Tile[]) puzzle[1])[1].getValue()) + (((Tile[]) puzzle[2])[1].getValue()) + (((Tile[]) puzzle[3])[0].getValue()),
			(((Tile[]) puzzle[0])[2].getValue()) + (((Tile[]) puzzle[1])[2].getValue()) + (((Tile[]) puzzle[2])[2].getValue()) + (((Tile[]) puzzle[3])[1].getValue()) + (((Tile[]) puzzle[4])[0].getValue()),
			(((Tile[]) puzzle[1])[3].getValue()) + (((Tile[]) puzzle[2])[3].getValue()) + (((Tile[]) puzzle[3])[2].getValue()) + (((Tile[]) puzzle[4])[1].getValue()),
			(((Tile[]) puzzle[2])[4].getValue()) + (((Tile[]) puzzle[3])[3].getValue()) + (((Tile[]) puzzle[4])[2].getValue()),
			(((Tile[]) puzzle[2])[0].getValue()) + (((Tile[]) puzzle[3])[0].getValue()) + (((Tile[]) puzzle[4])[0].getValue()),
			(((Tile[]) puzzle[1])[0].getValue()) + (((Tile[]) puzzle[2])[1].getValue()) + (((Tile[]) puzzle[3])[1].getValue()) + (((Tile[]) puzzle[4])[1].getValue()),
			(((Tile[]) puzzle[0])[0].getValue()) + (((Tile[]) puzzle[1])[1].getValue()) + (((Tile[]) puzzle[2])[2].getValue()) + (((Tile[]) puzzle[3])[2].getValue()) + (((Tile[]) puzzle[4])[2].getValue()),
			(((Tile[]) puzzle[0])[1].getValue()) + (((Tile[]) puzzle[1])[2].getValue()) + (((Tile[]) puzzle[2])[3].getValue()) + (((Tile[]) puzzle[3])[3].getValue()),
			(((Tile[]) puzzle[0])[2].getValue()) + (((Tile[]) puzzle[1])[3].getValue()) + (((Tile[]) puzzle[2])[4].getValue())
		};
		boolean solved = true;
		for(int val : puzzleSums) {
			if(val != 38) {
				solved = false;
			}
		}
		if(solved) { 
			Print.ln("Congratulations!\n\tYou solved the puzzle!");
			lastCommand = "quit";
		} else {
			Print.ln("Puzzle pattern is incorrect");
		}
	}
	public static void help() {
		Print.ln(	"Help:\n\t" + 
					"Commands:\n\t\t" +
					"help\t- You're viewing it\n\t\t" +
					"view\t- Usage:\n\t\t\t" +
					"view_board\t- View the current board\n\t\t\t" +
					"view_tiles\t- View the current tiles and their status\n\t\t" +
					"place\t- Usage:\n\t\t\t" +
					"place_TILE_at_ROW_COLUMN\n\t\t" +
					"remove\t- Usage:\n\t\t\t" +
					"remove_TILE_from_ROW_COLUMN\n\t\t" +
					"check\t- Check if you have the correct solution");
	}
	public static void trueMain() {
		if(!lastCommand.toLowerCase().equals("quit")) {
			parseString((String) Prompt.prompt("Do what?: ", input, "line"));
			trueMain();
		}
	}
}

/*
 * Part of my improvements library
 */

class Print {
	public static void ln(Object arg0) {
		System.out.println(arg0);
	}
	public static void ln(Object[] arg0) {
		System.out.println(arg0);
	}
	public static void p(Object arg0) {
		System.out.print(arg0);
	}
	public static void p(Object[] arg0) {
		System.out.print(arg0);
	}
}

class Prompt {
	public static Object prompt(String _prompt, Scanner input, Object type) {
		Print.p(_prompt);
		Object rv = new Error();
		boolean t = true;
		if(type instanceof Integer)
			if(input.hasNextInt())
				rv = input.nextInt();
		else if(type instanceof String)
			if(((String)type).toLowerCase().equals("line"))
				if(input.hasNextLine())
					rv = input.nextLine();
			else
				if(input.hasNext())
					rv = input.next();
		else if(type instanceof Boolean)
			if(input.hasNextBoolean())
				rv = input.nextBoolean();
		else if(type instanceof Float)
			if(input.hasNextFloat())
				rv = input.nextFloat();
		else if(type instanceof Double)
			if(input.hasNextDouble())
				rv = input.nextDouble();
		while(rv instanceof Error && t) {
			if(type instanceof Integer)
				rv = input.nextInt();
			else if(type instanceof String)
				rv = input.next();
			else if(type instanceof Boolean)
				rv = input.nextBoolean();
			else if(type instanceof Float)
				rv = input.nextFloat();
			else if(type instanceof Double)
				rv = input.nextDouble();
			t = false;
		}
		return rv;
	}
}