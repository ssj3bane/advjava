package tcayer.classwork.Course;

import tcayer.improvements.Print;

public class LabCourse extends Course {
	public static final String[] ALLOWED_DEPARTMENTS = new String[] {
		"CSC", "SCI", "PHY"
	};
	private String department;
	private int courseNumber;
	private int courseCredits;
	private double courseCost;
	public LabCourse() {
		super("0", 0, 0);
		updateCourseCost();
	}
	public LabCourse(String department, int courseNumber, int courseCredits) {
		super(department, courseNumber, courseCredits);
		updateCourseCost();
	}
	public double getCourseCost() {
		updateCourseCost();
		return courseCost;
	}
	public void updateCourseCost() {
		this.courseCost = (getCourseCredits() * 120) + 50;
	}
	public String toString() {
		return "" +
				"Department: " + getDepartment() +
				"\nCourse Number: " + getCourseNumber() +
				"\nCourse Credits: " + getCourseCredits() +
				"\nCourse Cost: " + getCourseCost();
	}
	public boolean equals(LabCourse compareTo) {
		return ((getDepartment().equals(compareTo.getDepartment())) &&
				(getCourseNumber() == compareTo.getCourseNumber()) &&
				(getCourseCredits() == compareTo.getCourseCredits()) &&
				(getCourseCost() == compareTo.getCourseCost()));
	}
}
