package tcayer.classwork.Course;

import tcayer.improvements.ArrayUtil;
import tcayer.improvements.Print;

public class Course {
	public final String[] ALLOWED_DEPARTMENTS = new String[] {
		"ENG", "MTH", "CSC", "HST", "HUM", "SCI", "LAN", "PHY"
	};
	private String department;
	private int courseNumber;
	private int courseCredits;
	private double courseCost;
	public Course() {
		setDepartment("0");
		setCourseNumber(0);
		setCourseCredits(0);
		updateCourseCost();
	}
	public Course(String department, int courseNumber, int courseCredits) {
		setDepartment(department);
		setCourseNumber(courseNumber);
		setCourseCredits(courseCredits);
		updateCourseCost();
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		if(ArrayUtil.includes(ALLOWED_DEPARTMENTS, department)) {
			this.department = department;
		} else {
			Print.ln("Department is not valid");
		}
	}
	public int getCourseNumber() {
		return courseNumber;
	}
	public void setCourseNumber(int courseNumber) {
		if(courseNumber >= 100 && courseNumber < 500) {
			this.courseNumber = courseNumber;
		} else {
			Print.ln("Course Number is not valid");
		}
	}
	public int getCourseCredits() {
		return courseCredits;
	}
	public void setCourseCredits(int courseCredits) {
		if((courseCredits > 0 && courseCredits < 5) && !(courseCredits == 2)) {
		this.courseCredits = courseCredits;
		} else {
			Print.ln("Course credit amount is invalid");
		}
	}
	public double getCourseCost() {
		updateCourseCost();
		return courseCost;
	}
	public void updateCourseCost() {
		this.courseCost = getCourseCredits() * 120;
	}
	public String toString() {
		return "" +
				"Department: " + getDepartment() +
				"\nCourse Number: " + getCourseNumber() +
				"\nCourse Credits: " + getCourseCredits() +
				"\nCourse Cost: " + getCourseCost();
	}
	public boolean equals(Course compareTo) {
		return ((getDepartment().equals(compareTo.getDepartment())) &&
				(getCourseNumber() == compareTo.getCourseNumber()) &&
				(getCourseCredits() == compareTo.getCourseCredits()) &&
				(getCourseCost() == compareTo.getCourseCost()));
	}
}
