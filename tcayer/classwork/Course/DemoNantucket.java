package tcayer.classwork.Course;

import tcayer.improvements.ArrayUtil;
import tcayer.improvements.Print;
import tcayer.improvements.Prompt;
import java.util.Scanner;

public class DemoNantucket {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int courseAmt = (int) Prompt.prompt("How many courses are you taking: ", input, 1);
		Course[] courses = new Course[courseAmt];
		for(int i = 0; i < courseAmt; i++) {
			String dpt = (String) Prompt.prompt("What department is the course: ", input, "1");
			int courseNumber = (int) Prompt.prompt("What is the course number: ", input, 1);
			int courseCredits = (int) Prompt.prompt("How many credits is the course: ", input, 1);
			if(ArrayUtil.includes(LabCourse.ALLOWED_DEPARTMENTS, dpt)) {
				courses[i] = new LabCourse(dpt, courseNumber, courseCredits);
			} else {
				courses[i] = new Course(dpt, courseNumber, courseCredits);
			}
		}
		float totalCost = 0;
		for(Object course : courses) {
			if(course instanceof Course) {
				Print.ln(course.toString());
				totalCost += ((Course) course).getCourseCost();
			} else if(course instanceof LabCourse) {
				Print.ln(course.toString());
				totalCost += ((LabCourse) course).getCourseCost();
			}
		}
		Print.ln("Total Cost: " + totalCost);
	}
}
