package tcayer.classwork.C9PP2;

import java.util.Scanner;

import tcayer.classwork.C9PP2.Calculator.DivideByZeroException;
import tcayer.classwork.C9PP2.Calculator.UnknownOpException;

public class C9PP2 {
	public static void main(String[] args) throws DivideByZeroException, UnknownOpException {
		ExtendedCalculator myCalc = new ExtendedCalculator();
		myCalc.doCalculation();
	}
}
/*
 * From listing 9.12
 */
class Calculator {
	private double result;
	private double precision = 0.001;
	public Calculator() {
		result = 0;
	}
	public void handleDivideByZeroException(DivideByZeroException e) {
		System.out.println("Dividing by zero.");
		System.out.println("Program aborted");
		System.exit(0);
	}
	public void handleUnknownOpException(UnknownOpException e) {
		System.out.println(e.getMessage());
		System.out.println("Try again from the beginning:");
		try {
			System.out.print("Format of each line: ");
			System.out.println("operator number");
			System.out.println("For example: + 3");
			System.out.println("To end, enter the letter e.");
			doCalculation();
		}
		catch(UnknownOpException e2) {
			System.out.println(e2.getMessage());
			System.out.println("Try again at some other time.");
			System.out.println("Program ending.");
			System.exit(0);
		}
		catch(DivideByZeroException e3) {
			handleDivideByZeroException(e3);
		}
	}
	public void reset() {
		result = 0;
	}
	public void setResult(double newResult) {
		result = newResult;
	}
	public double getResult() {
		return result;
	}
	public double evaluate(char op, double n1, double n2) throws DivideByZeroException, UnknownOpException {
		double answer;
		switch(op) {
		case '+':
			answer = n1 + n2;
			break;
		case '-':
			answer = n1 - n2;
			break;
		case '*':
			answer = n1 - n2;
			break;
		case '/':
			if((-precision < n2) && (n2 < precision))
				throw new DivideByZeroException();
			answer = n1 / n2;
			break;
		default:
			throw new UnknownOpException(op);
		}
		return answer;
	}
	public void doCalculation() throws DivideByZeroException, UnknownOpException {
		Scanner keyboard = new Scanner(System.in);
		boolean done = false;
		result = 0;
		System.out.println("result = " + result);
		while(!done) {
			char nextOp = (keyboard.next()).charAt(0);
			if((nextOp == 'e') || (nextOp == 'E'))
				done = true;
			else {
				double nextNumber = keyboard.nextDouble();
				result = evaluate(nextOp, result, nextNumber);
				System.out.println("result " + nextOp + " " + nextNumber + " = " + result);
				System.out.println("updated result = " + result);
			}
		}
	}
	class UnknownOpException extends Exception {
		public UnknownOpException() {
			super("UnknownOpException");
		}
		public UnknownOpException(char op) {
			super(op + " is an unknown operator.");
		}
		public UnknownOpException(String message) {
			super(message);
		}
	}
	class DivideByZeroException extends Exception {
		public DivideByZeroException() {
			super("Dividing by Zero!");
		}
		public DivideByZeroException(String message) {
			super(message);
		}
	}
}
/*
 * m - save in memory; memory = result
 * r - recall memory; displays value of memory, no change to result
 * c - clear; result = 0
 * e - end
 */
class ExtendedCalculator extends Calculator {
	private double memory;
	public void setMemory(double value) {
		memory = value;
	}
	public double getMemory() {
		return memory;
	}
	@Override
	public void doCalculation() throws DivideByZeroException, UnknownOpException {
		Scanner keyboard = new Scanner(System.in);
		boolean done = false;
		setResult(0);
		System.out.println("result = " + getResult());
		while(!done) {
			char nextOp = (keyboard.next()).charAt(0);
			if((nextOp == 'e') || (nextOp == 'E'))
				done = true;
			else if(Character.toLowerCase(nextOp) == 'm') {
				setMemory(getResult());
				System.out.println("updated memory = " + getMemory());
			} else if(Character.toLowerCase(nextOp) == 'c') {
				reset();
			} else if(Character.toLowerCase(nextOp) == 'r')
				System.out.println("memory = " + getMemory());
			else {
				double nextNumber = keyboard.nextDouble();
				setResult(evaluate(nextOp, getResult(), nextNumber));
				System.out.println("result " + nextOp + " " + nextNumber + " = " + getResult());
				System.out.println("updated result = " + getResult());
			}
		}
	}
}