package tcayer.classwork.Horses;

import java.util.Scanner;

import tcayer.improvements.Print;
import tcayer.improvements.Prompt;

public class DemoHorses {
	public static Horse[] horses = new Horse[6];
	public static void main(String[] args) {
		Print.ln("For the simplicity of the problem, the mother and father horses\nwill be the same for all breeding horses.");
		Horse mother = new Horse("Karen", "Brown", 1999);
		Horse father = new Horse("Keith", "Beige", 2000);
		Scanner input = new Scanner(System.in);
		for(int i = 0; i < 6; i++) {
			if(i < 3) {
				horses[i] = new RaceHorse((String) Prompt.prompt("Horse name: ", input, "1"), (String) Prompt.prompt("Horse color: ", input, "1"), (int) Prompt.prompt("Birth Year: ", input, 1), (int) Prompt.prompt("Races entered: ", input, 1), (int) Prompt.prompt("Races won: ", input, 1));
			} else {
				horses[i] = new BreedingHorse((String) Prompt.prompt("Horse name: ", input, "1"), (String) Prompt.prompt("Horse color: ", input, "1"), (int) Prompt.prompt("Birth Year: ", input, 1), mother, father);
			}
		}
		for(Horse horse : horses) {
			if(horse instanceof RaceHorse) {
				Print.ln(((RaceHorse) horse).toString());
			} else if(horse instanceof BreedingHorse) {
				Print.ln(((BreedingHorse) horse).toString());
			}
		}
	}
}
