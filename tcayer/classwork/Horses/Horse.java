package tcayer.classwork.Horses;

import java.util.Scanner;

import tcayer.improvements.Print;
import tcayer.improvements.Prompt;

public class Horse {
	private String name;
	private String color;
	private int birthYear;
	public Horse() {
		setName("");
		setColor("");
		setBirthYear(1988);
	}
	public Horse(String _name, String _color, int _birthYear) {
		setName(_name);
		setColor(_color);
		setBirthYear(_birthYear);
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public int getBirthYear() {
		return birthYear;
	}
	public void setBirthYear(int __birthYear) {
		int _birthYear = __birthYear;
		Scanner input = new Scanner(System.in);
		while(_birthYear < 1988 || _birthYear > 2018) {
			Print.ln("Invalid entry");
			_birthYear = (int) Prompt.prompt("Enter birth year: ", input, 1);
		}
		this.birthYear = _birthYear;
	}
	public String toString() {
		return "" +
				"Name: " + getName() +
				"\nColor: " + getColor() +
				"\nBirth Year: " + getBirthYear();
	}
	public boolean equals(Horse compareTo) {
		if(compareTo instanceof Horse) {
			return ((getName().equals(compareTo.getName())) &&
					(getColor().equals(compareTo.getColor())) &&
					(getBirthYear() == compareTo.getBirthYear()));
		} else {
			return false;
		}
	}
}
