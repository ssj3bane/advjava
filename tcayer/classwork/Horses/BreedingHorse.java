package tcayer.classwork.Horses;

public class BreedingHorse extends Horse {
	private Horse mother;
	private Horse father;
	public BreedingHorse() {
		super();
		setMother(new Horse());
		setFather(new Horse());
	}
	public BreedingHorse(String _name, String _color, int _birthYear, Horse mother, Horse father) {
		super(_name, _color, _birthYear);
		setMother(mother);
		setFather(father);
	}
	public String toString() {
		return "" + "Name: " + getName() +
				"\nColor: " + getColor() +
				"\nBirth Year: " + getBirthYear() +
				"\nMother: \n\t" + getMother().toString().replace("\n", "\n\t") +
				"\nFather: \n\t" + getFather().toString().replace("\n", "\n\t");
	}
	public boolean equals(Horse compareTo) {
		if(compareTo instanceof BreedingHorse) {
			return ((getName().equals(compareTo.getName())) &&
					(getColor().equals(compareTo.getColor())) &&
					(getBirthYear() == compareTo.getBirthYear()) &&
					(getMother().equals(((BreedingHorse) compareTo).getMother()) &&
					(getFather().equals(((BreedingHorse) compareTo).getFather()))));
		} else {
			return false;
		}
	}
	public Horse getMother() {
		return mother;
	}
	public void setMother(Horse mother) {
		this.mother = mother;
	}
	public Horse getFather() {
		return father;
	}
	public void setFather(Horse father) {
		this.father = father;
	}
}
