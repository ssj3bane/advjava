package tcayer.classwork.Horses;

import java.util.Scanner;

import tcayer.improvements.Print;
import tcayer.improvements.Prompt;

public class RaceHorse extends Horse {
	private int races;
	private int wins;
	public RaceHorse() {
		super();
		setRaces(0);
		setWins(0);
	}
	public RaceHorse(String _name, String _color, int _birthYear, int races, int wins) {
		super(_name, _color, _birthYear);
		setRaces(races);
		setWins(wins);
	}
	public void setRaces(int _races) {
		Scanner input = new Scanner(System.in);
		while(_races < 0) {
			Print.ln("Invalid entry");
			_races = (int) Prompt.prompt("Enter # of races: ", input, 1);
		}
		this.races = _races;
	}
	public int getRaces() {
		return this.races;
	}
	public void setWins(int _wins) {
		Scanner input = new Scanner(System.in);
		while(_wins < 0 || _wins > this.races) {
			Print.ln("Invalid entry");
			_wins = (int) Prompt.prompt("Enter # of wins: ", input, 1);
		}
		this.wins = _wins;
	}
	public int getWins() {
		return this.wins;
	}
	public String toString() {
		return "" + "Name: " + getName() +
				"\nColor: " + getColor() +
				"\nBirth Year: " + getBirthYear() +
				"\nRaces: " + getRaces() +
				"\nWins: " + getWins();
	}
	public boolean equals(Horse compareTo) {
		if(compareTo instanceof RaceHorse) {
			return ((getName().equals(compareTo.getName())) &&
					(getColor().equals(compareTo.getColor())) &&
					(getBirthYear() == compareTo.getBirthYear()) &&
					(getWins() == ((RaceHorse) compareTo).getWins()) &&
					(getRaces() == ((RaceHorse) compareTo).getRaces()));
		} else {
			return false;
		}
	}
}
