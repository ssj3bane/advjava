package tcayer.improvements;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class UpdateGit {
	public static void main(String[] args) throws IOException, ParseException {
		update(getDate());
	}
	public static String getDate() throws ParseException {
		SimpleDateFormat myDate = new SimpleDateFormat("MM-dd-YY");
		Date date = new Date();
		return myDate.format(date);
	}
	public static void update(String commitMessage) throws IOException {
		String[] commands = new String[] {
				"cmd",
				"/c",
				"start",
				"cmd.exe",
				"/K",
				"\"cd src && git add . && git commit -m \"" + commitMessage + "\" && git push -u origin master && exit\""
		};
		Process myProcess = Runtime.getRuntime().exec(commands);
	}
}
