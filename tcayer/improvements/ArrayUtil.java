package tcayer.improvements;

import java.util.Arrays;

public class ArrayUtil {
	public static String[] sort(String[] array, int direction) {
		String[] returnValue = copy(array);
		Arrays.sort(returnValue);
		String[] backupArray = copy(returnValue);
		if(direction == -1) {
			for(int i = returnValue.length-1; i >= 0; i--) {
				returnValue[i] = backupArray[(returnValue.length-1)-i];
			}
		}
		return returnValue;
	}
	public static String[] copy(String[] array) {
		String[] returnValue = new String[array.length];
		for(int i = 0; i < returnValue.length; i++) {
			returnValue[i] = array[i];
		}
		return returnValue;
	}
	public static int[] sort(int[] array, int direction) {
		int[] returnValue = copy(array);
		Arrays.sort(returnValue);
		int[] backupArray = copy(returnValue);
		if(direction == -1) {
			for(int i = returnValue.length-1; i >= 0; i--) {
				returnValue[i] = backupArray[(returnValue.length-1)-i];
			}
		}
		return returnValue;
	}
	public static int[] copy(int[] array) {
		int[] returnValue = new int[array.length];
		for(int i = 0; i < returnValue.length; i++) {
			returnValue[i] = array[i];
		}
		return returnValue;
	}
	public static Object[] sort(Object[] array, int direction) {
		Object[] returnValue = copy(array);
		Arrays.sort(returnValue);
		Object[] backupArray = copy(returnValue);
		if(direction == -1) {
			for(int i = returnValue.length-1; i >= 0; i--) {
				returnValue[i] = backupArray[(returnValue.length-1)-i];
			}
		}
		return returnValue;
	}
	public static Object[] copy(Object[] array) {
		Object[] returnValue = new Object[array.length];
		for(int i = 0; i < returnValue.length; i++) {
			returnValue[i] = array[i];
		}
		return returnValue;
	}
	public static boolean includes(Object[] array, Object check) {
		boolean rv = false;
		for(Object el : array) {
			if(el.equals(check)) {
				rv = true;
			}
		}
		return rv;
	}
}