package tcayer.improvements;

public class Print {
	/**
	 * Print a new line of arg0
	 * @param arg0
	 */
	public static void ln(Object arg0) {
		System.out.println(arg0);
	}
	/**
	 * Print a new line of arg0
	 * @param arg0
	 */
	public static void ln(Object[] arg0) {
		System.out.println(arg0);
	}
	/**
	 * Print arg0
	 * @param arg0
	 */
	public static void p(Object arg0) {
		System.out.print(arg0);
	}
	/**
	 * Print arg0
	 * @param arg0
	 */
	public static void p(Object[] arg0) {
		System.out.print(arg0);
	}
}