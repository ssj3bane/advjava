package tcayer.improvements;

import java.util.Scanner;

public class Prompt {
	private Scanner input;
	public Prompt() {
		input = new Scanner(System.in);
	}

	/**
	 * A simple prompt. Ask and ye shall receive.
	 * @param prompt - String to send to System.out
	 * @param input - Scanner to get .next...() from
	 * @param type - Any Object, output will be same as the type
	 * @return Result from input respective to the inputed type
	 */
	public static Object prompt(String prompt, Scanner input, Object type) {
		Print.p(prompt);
		Object rv = new Error();
		boolean t = true;
		if(type instanceof Integer)
			if(input.hasNextInt())
				rv = input.nextInt();
		else if(type instanceof String)
			if(((String)type).toLowerCase().equals("line"))
				if(input.hasNextLine())
					rv = input.nextLine();
			else
				if(input.hasNext())
					rv = input.next();
		else if(type instanceof Boolean)
			if(input.hasNextBoolean())
				rv = input.nextBoolean();
		else if(type instanceof Float)
			if(input.hasNextFloat())
				rv = input.nextFloat();
		else if(type instanceof Double)
			if(input.hasNextDouble())
				rv = input.nextDouble();
		else if(type instanceof Long)
			if(input.hasNextLong())
				rv = input.nextLong();
		while(rv instanceof Error && t) {
			if(type instanceof Integer)
				rv = input.nextInt();
			else if(type instanceof String)
				rv = input.next();
			else if(type instanceof Boolean)
				rv = input.nextBoolean();
			else if(type instanceof Float)
				rv = input.nextFloat();
			else if(type instanceof Double)
				rv = input.nextDouble();
			else if(type instanceof Long)
				rv = input.nextLong();
			t = false;
		}
		return rv;
	}
	public Object prompt(String prompt, Object type) {
		Print.p(prompt);
		Object rv = new Error();
		boolean t = true;
		if(type instanceof Integer)
			if(input.hasNextInt())
				rv = input.nextInt();
		else if(type instanceof String)
			if(((String)type).toLowerCase().equals("line"))
				if(input.hasNextLine())
					rv = input.nextLine();
			else
				if(input.hasNext())
					rv = input.next();
		else if(type instanceof Boolean)
			if(input.hasNextBoolean())
				rv = input.nextBoolean();
		else if(type instanceof Float)
			if(input.hasNextFloat())
				rv = input.nextFloat();
		else if(type instanceof Double)
			if(input.hasNextDouble())
				rv = input.nextDouble();
		else if(type instanceof Long)
			if(input.hasNextLong())
				rv = input.nextLong();
		while(rv instanceof Error && t) {
			if(type instanceof Integer)
				rv = input.nextInt();
			else if(type instanceof String)
				rv = input.next();
			else if(type instanceof Boolean)
				rv = input.nextBoolean();
			else if(type instanceof Float)
				rv = input.nextFloat();
			else if(type instanceof Double)
				rv = input.nextDouble();
			else if(type instanceof Long)
				rv = input.nextLong();
			t = false;
		}
		return rv;
	}
	public void close() {
		input.close();
	}
}
