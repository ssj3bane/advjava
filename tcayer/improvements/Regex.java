package tcayer.improvements;

import java.util.regex.Pattern;

public class Regex {
	public static Boolean match(String regex, String matchedText) {
		return Pattern.compile(regex).matcher(matchedText).find();
	}
}
