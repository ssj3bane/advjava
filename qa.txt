true|All Java classes extend Object
true|In an instance method or a constructor, "this" is a reference to the current object
false|ArrayList is a primitive
false|Encapsulation means the same thing as information hiding
true|Every Java program requires at least one class
false|In a class declaration, the opening left brace { and the closing right brace } must occur alone on a line
false|Every source file must be named after the first class declared in the file
true|Every class file belongs to a package
false|A package statement can occur either before or after import statements
false|Java has standard packages but does not allow programmer-defined packages